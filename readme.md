#Goal
Realise SPA, show tech stack knowledge. 

#XHR
Xhr directory has got JSON stub files instead API.

Need to integrate each interface.

All requests must run by POST, if it's necessary then a data must pass as HTTP body as JSON (at your discretion).
 
Need to realise SPD for requests handling and write working handles, preferably to document / comment on all.

Interfaces and descriptions:

* login (login.json)
* get friends list (friend/all.json)
* friendship acceptation (friend/accept.json)
* friendship cancellation (friend/decline.json)
* getting inbox friendship requests (friend/inbox.json)
* getting outbox friendship requests (friend/outbox.json)
* send friendship request (friend/add.json)
* remove friendship (friend/remove.json)
* get user profile (profile.json)
* search a user (search.json)

Need to use the follow tech stack:

* JQuery
* Backbone
* Jade
* Less

You need to create git repo on any public service and develop project with it.

Frequency of comments will be rated.

You can spend less that 8 hours.

SPA has to contain:

* Navigation menu with active option selection
* Page with user profile
* Page with friendship requests and managing friends
* Page with user searching
* Auth page
