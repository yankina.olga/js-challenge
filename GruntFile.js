module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        // Задания
        pug: {
            options: {
                data: {
                    debug: false
                }
            },
            development: {
                files: [{
                    expand: true,
                    cwd: 'markup/jade/',
                    src: '*.jade',
                    dest: 'markup/',
                    ext: '.html'
                }],
            }
        },
        less: {
            development: {
                files: [{
                    expand: true,
                    cwd: 'markup/less/',
                    src: 'global.less',
                    dest: 'markup/css',
                    ext: '.css'
                }],
            }
        },
        concat: {
            options: {
            },
            development: {
                src: ['markup/js/*.js', 'markup/blocks/**/*.js', '!markup/js/main.js'],
                dest: 'markup/js/main.js',
            },
        },
        browserify: {
            options: {
                transform: ['jadeify']
            },
            development: {
                src: 'markup/js/main.js',
                dest: 'markup/js/app/app.js'
            }
        },
        watch: {
            options: {
                livereload: true,
                //spawn: false,
                interrupt: true
            },
            scripts: {
                cwd: 'markup/',
                files: ['**/*.jade','**/*.less'],
                tasks: ['compile_jade','compile_less','compile_concat','compile_browserify'],
            },
            js: {
                files: ['markup/js/*.js', 'markup/blocks/**/*.js' ,'!markup/js/main.js'],
                tasks: ['compile_concat','compile_browserify']
            }
        },
    });

    // Load the plugins
    grunt.loadNpmTasks('grunt-contrib-pug');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-browserify');


    // Default task(s).
    grunt.registerTask('compile_jade', ['pug:development']);
    grunt.registerTask('compile_less', ['less:development']);
    grunt.registerTask('compile_concat', ['concat:development']);
    grunt.registerTask('compile_browserify', ['browserify:development']);
};