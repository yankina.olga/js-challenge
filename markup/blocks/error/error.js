function testError() {
    myUtils.ajax("../xhr/error-test.json", function (obj) {});
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="error"]', function (evt) {
        evt.preventDefault();

        testError();
    })
});