// getting outbox friendship requests

// Подключение шаблона всей страницы
var templateFriendsPage = require("../jade/friends.jade");

// Подключение шаблона Outbox
var templateOutbox = require("../blocks/friends-outbox/friends-outbox.jade");

function outbox() {
    myUtils.ajax("../xhr/friend/outbox.json", function(obj) {

        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("friends-outbox").innerHTML = templateOutbox(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="friends"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('friends', 'Friends page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateFriendsPage();
        outbox();
    });

});
