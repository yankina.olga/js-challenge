// getting inbox friendship requests

// Подключение шаблона всей страницы
var templateFriendsPage = require("../jade/friends.jade");

// Подключение шаблона Inbox
var templateInbox = require("../blocks/friends-inbox/friends-inbox.jade");

function inbox() {
    myUtils.ajax("../xhr/friend/inbox.json", function(obj) {
        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("friends-inbox").innerHTML = templateInbox(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="friends"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('friends', 'Friends page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateFriendsPage();
        inbox();
    });

});