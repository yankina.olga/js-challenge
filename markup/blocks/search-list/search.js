// search a user

// Подключение шаблона всей страницы
var templateSearchPage = require("../jade/search.jade");

// Подключение шаблона поиска
var templateSearch = require("../blocks/search-list/search-list.jade");

function search() {
    myUtils.ajax("../xhr/search.json", function(obj) {

        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("search-list").innerHTML = templateSearch(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="search"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('search', 'Search page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateSearchPage();
        search();
    });

});