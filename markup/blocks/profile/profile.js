// get user profile

// Подключение шаблона всей страницы
var templateProfilePage = require("../jade/profile.jade");

// Подключение шаблона с профилем
var templateProfile = require("../blocks/profile/profile-person.jade");

function profile() {
    myUtils.ajax("../xhr/profile.json", function(obj) {

        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("profile-person").innerHTML = templateProfile(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="profile"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('profile', 'Profile page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateProfilePage();
        profile();
    });

    // Переход со страницы авторизации
    $('#login').on('click', function(evt) {
        document.getElementById("pageContainer").innerHTML = templateProfilePage();
        window.history.pushState('profile', 'Profile page', $(this).attr('href'));

        profile();
    });


});