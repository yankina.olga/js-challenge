// friendship cancellation

// Получение json файла
function friendshipDecline(elem) {
    myUtils.ajax("../xhr/friend/decline.json", function(obj) {
        if(obj.success == true) {
            alert('Decline');

            // Удаление элемента из списка
            $(elem).parents('li').remove();
        }
    });
}

$(document).ready(function() {
    // Аналог бывшей функци live в jquery
    $(document).on('click',  '.fa.fa-times', function(evt) {
        evt.preventDefault();

        friendshipDecline(evt.target);
    });
});
