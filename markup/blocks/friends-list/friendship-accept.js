// friendship acceptation

var templateFriendsLine = require("../blocks/friends-list/friend-line.jade");

function friendshipAccept(elem) {
    myUtils.ajax("../xhr/friend/accept.json", function(obj) {

        if(obj.success == true) {
            alert('Accept!');
            var userName = $(elem).parents('li').find('span').html();
            userName = userName.split(' ');

            // Добавление в список Friends
            $('#friends-list ul').append(templateFriendsLine({"userData" : {
                "name": userName[0],
                "surname": userName[1]
            }}));

            $(elem).parents('li').remove();
        }
    });

}

$(document).ready(function() {
    // Аналог бывшей функции live в jquery
    $(document).on('click',  '.fa.fa-check', function(evt) {
        evt.preventDefault();
        friendshipAccept(evt.target);
    });
});

