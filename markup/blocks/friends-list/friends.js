// get friends list

// Подключение шаблона всей страницы
var templateFriendsPage = require("../jade/friends.jade");

// // Подключение шаблона списка друзей
var templateFriends = require("../blocks/friends-list/friends-list.jade");

function loadFriends() {
  myUtils.ajax("../xhr/friend/all.json", function(obj) {

      // Наполнение шаблона данными с сервера и подстановка в публичную часть
      document.getElementById("friends-list").innerHTML = templateFriends(obj);
  });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="friends"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('friends', 'Friends page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateFriendsPage();
        loadFriends();
    });

});




