// Вспомогательный класс

var Utils = function() {
};

// Отправка ajax-запроса
Utils.prototype.ajax = function(url, callback) {
    var ajaxReq = new XMLHttpRequest();
    var self = this;

    ajaxReq.ontimeout = function () {
        self.showError('Время ожидания запроса истекло');

    };

    ajaxReq.onreadystatechange = function() {
        if (this.readyState != 4) return;

        if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;
            var obj = JSON.parse(response);
            callback(obj);
        } else {
            self.showError('Ошибка ответа сервера');
        }

    }

    ajaxReq.open("POST", url, true);
    ajaxReq.timeout = 30000;
    try {
        ajaxReq.send();
    } catch (e) {

    }

};

Utils.prototype.showError = function(errorText) {
    $('#error').css('display', 'block');
    $('#error').html(errorText);
}

var myUtils = new Utils();



