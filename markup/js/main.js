// Вспомогательный класс

var Utils = function() {
};

// Отправка ajax-запроса
Utils.prototype.ajax = function(url, callback) {
    var ajaxReq = new XMLHttpRequest();
    var self = this;

    ajaxReq.ontimeout = function () {
        self.showError('Время ожидания запроса истекло');

    };

    ajaxReq.onreadystatechange = function() {
        if (this.readyState != 4) return;

        if (this.readyState == 4 && this.status == 200) {
            var response = this.responseText;
            var obj = JSON.parse(response);
            callback(obj);
        } else {
            self.showError('Ошибка ответа сервера');
        }

    }

    ajaxReq.open("POST", url, true);
    ajaxReq.timeout = 30000;
    try {
        ajaxReq.send();
    } catch (e) {

    }

};

Utils.prototype.showError = function(errorText) {
    $('#error').css('display', 'block');
    $('#error').html(errorText);
}

var myUtils = new Utils();




function testError() {
    myUtils.ajax("../xhr/error-test.json", function (obj) {});
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="error"]', function (evt) {
        evt.preventDefault();

        testError();
    })
});
// getting inbox friendship requests

// Подключение шаблона всей страницы
var templateFriendsPage = require("../jade/friends.jade");

// Подключение шаблона Inbox
var templateInbox = require("../blocks/friends-inbox/friends-inbox.jade");

function inbox() {
    myUtils.ajax("../xhr/friend/inbox.json", function(obj) {
        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("friends-inbox").innerHTML = templateInbox(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="friends"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('friends', 'Friends page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateFriendsPage();
        inbox();
    });

});
// get friends list

// Подключение шаблона всей страницы
var templateFriendsPage = require("../jade/friends.jade");

// // Подключение шаблона списка друзей
var templateFriends = require("../blocks/friends-list/friends-list.jade");

function loadFriends() {
  myUtils.ajax("../xhr/friend/all.json", function(obj) {

      // Наполнение шаблона данными с сервера и подстановка в публичную часть
      document.getElementById("friends-list").innerHTML = templateFriends(obj);
  });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="friends"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('friends', 'Friends page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateFriendsPage();
        loadFriends();
    });

});





// friendship acceptation

var templateFriendsLine = require("../blocks/friends-list/friend-line.jade");

function friendshipAccept(elem) {
    myUtils.ajax("../xhr/friend/accept.json", function(obj) {

        if(obj.success == true) {
            alert('Accept!');
            var userName = $(elem).parents('li').find('span').html();
            userName = userName.split(' ');

            // Добавление в список Friends
            $('#friends-list ul').append(templateFriendsLine({"userData" : {
                "name": userName[0],
                "surname": userName[1]
            }}));

            $(elem).parents('li').remove();
        }
    });

}

$(document).ready(function() {
    // Аналог бывшей функции live в jquery
    $(document).on('click',  '.fa.fa-check', function(evt) {
        evt.preventDefault();
        friendshipAccept(evt.target);
    });
});


// friendship cancellation

// Получение json файла
function friendshipDecline(elem) {
    myUtils.ajax("../xhr/friend/decline.json", function(obj) {
        if(obj.success == true) {
            alert('Decline');

            // Удаление элемента из списка
            $(elem).parents('li').remove();
        }
    });
}

$(document).ready(function() {
    // Аналог бывшей функци live в jquery
    $(document).on('click',  '.fa.fa-times', function(evt) {
        evt.preventDefault();

        friendshipDecline(evt.target);
    });
});

// getting outbox friendship requests

// Подключение шаблона всей страницы
var templateFriendsPage = require("../jade/friends.jade");

// Подключение шаблона Outbox
var templateOutbox = require("../blocks/friends-outbox/friends-outbox.jade");

function outbox() {
    myUtils.ajax("../xhr/friend/outbox.json", function(obj) {

        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("friends-outbox").innerHTML = templateOutbox(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="friends"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('friends', 'Friends page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateFriendsPage();
        outbox();
    });

});

// get user profile

// Подключение шаблона всей страницы
var templateProfilePage = require("../jade/profile.jade");

// Подключение шаблона с профилем
var templateProfile = require("../blocks/profile/profile-person.jade");

function profile() {
    myUtils.ajax("../xhr/profile.json", function(obj) {

        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("profile-person").innerHTML = templateProfile(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="profile"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('profile', 'Profile page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateProfilePage();
        profile();
    });

    // Переход со страницы авторизации
    $('#login').on('click', function(evt) {
        document.getElementById("pageContainer").innerHTML = templateProfilePage();
        window.history.pushState('profile', 'Profile page', $(this).attr('href'));

        profile();
    });


});
// search a user

// Подключение шаблона всей страницы
var templateSearchPage = require("../jade/search.jade");

// Подключение шаблона поиска
var templateSearch = require("../blocks/search-list/search-list.jade");

function search() {
    myUtils.ajax("../xhr/search.json", function(obj) {

        // Наполнение шаблона данными с сервера и подстановка в публичную часть
        document.getElementById("search-list").innerHTML = templateSearch(obj);
    });
}

$(document).ready(function () {
    $(document).on('click', 'a[href$="search"]', function(evt) {
        evt.preventDefault();

        // Изменение адресной строки
        window.history.pushState('search', 'Search page', $(this).attr('href'));

        document.getElementById("pageContainer").innerHTML = templateSearchPage();
        search();
    });

});